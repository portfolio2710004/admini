// ===========================================================================
// Electron 설정
// ===========================================================================

const { app, BrowserWindow, ipcMain } = require('electron');

function createWindow() {
    let appWindow;

    appWindow = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false, // Electron 12 이상에서는 기본값이 true입니다.
            enableRemoteModule: true, // Electron 10 이후에는 필요에 따라 true로 설정
        }
        
    });

    appWindow.loadURL(`file://${__dirname}/dist/admini/browser/index.html`);

    appWindow.on('closed', () => {
        win = null;
    });
}

app.on('ready', createWindow);

const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('./database.sqlite');

ipcMain.on('form-submission', (event, formData) => {
    console.log(formData);
    db.run(`INSERT INTO admin (username, password) VALUES (?, ?)`, [formData.username, formData.password], function (err) {
        if (err) {
            return console.log(err.message);
        }
        console.log(`A row has been inserted with rowid ${this.lastID}`);
    });
});
