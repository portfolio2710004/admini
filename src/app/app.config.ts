import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';

import { ADMIN_SERVICE } from '../interfaces/admin/admin.service';
import { AUTH_SERVICE } from '../interfaces/auth/auth.service';

import { AdminService } from '../services/admin.service';
import { AuthService } from '../services/auth.service';

import { routes } from './app.routes';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    { provide: ADMIN_SERVICE, useClass: AdminService },
    { provide: AUTH_SERVICE, useClass: AuthService },
  ]
};
