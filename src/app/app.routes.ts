import { Routes } from '@angular/router';
import { HasAdminGuard } from '../guards/has-admin.guard';

export const routes: Routes = [
    { 
        path: '', 
        redirectTo: 'signin', 
        pathMatch: 'full' 
    },
    {
        canActivate: [HasAdminGuard],
        path:'signin',
        loadComponent: () => import('./pages/signin/signin.page').then(m => m.SigninPage)
    },
    {
        path: 'signup',
        loadComponent: () => import('./pages/signup/signup.page').then(m => m.SignupPage)
    }
];
