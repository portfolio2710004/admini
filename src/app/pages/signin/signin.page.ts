import { Component, inject } from "@angular/core";
import { AbstractControl, ReactiveFormsModule, Validators } from "@angular/forms";

import { passwordExp, usernameExp } from "../../../interfaces/admin/admin";
import { AUTH_SERVICE } from "../../../interfaces/auth/auth.service";
import { FormPage } from "../../../interfaces/page/form.page";

export type TSigninFormData = {
    username: string;
    password: string;
}

@Component({
    selector: 'app-signin',
    templateUrl: './signin.page.html',
    standalone: true,
    imports: [
        ReactiveFormsModule
    ]
})
export class SigninPage extends FormPage {

    private readonly authService = inject(AUTH_SERVICE);

    readonly username: AbstractControl;
    readonly password: AbstractControl;

    constructor() {
        super();

        this.formGroup = this.formBuilder.group({
            username: ["", [
                Validators.required,
                Validators.pattern(usernameExp)
            ]],
            password: ["", [
                Validators.required,
                Validators.pattern(passwordExp)
            ]],
        });

        this.username = this.getControls('username');
        this.password = this.getControls('password');
    }

    onSubmit() {
        const formData = this.formGroup.value as TSigninFormData;
        // this.authService.signin(formData.username, formData.password).subscribe(console.log);
    }
}