import { Component, inject } from "@angular/core";
import { AbstractControl, ReactiveFormsModule, Validators } from "@angular/forms";

import { passwordExp, usernameExp } from "../../../interfaces/admin/admin";
import { ADMIN_SERVICE } from "../../../interfaces/admin/admin.service";
import { FormPage } from "../../../interfaces/page/form.page";


@Component({
    selector: "signin-page",
    templateUrl: "./signup.page.html",
    standalone: true,
    imports: [
        ReactiveFormsModule,
    ]
})
export class SignupPage extends FormPage {

    private readonly adminService = inject(ADMIN_SERVICE);

    readonly username: AbstractControl;
    readonly password: AbstractControl;
    readonly rePassword: AbstractControl;

    constructor() {
        super();

        this.formGroup = this.formBuilder.group({
            username: ["", [
                Validators.required,
                Validators.pattern(usernameExp)
            ]],
            password: ["", [
                Validators.required,
                Validators.pattern(passwordExp)
            ]],
            rePassword: ["", [
                Validators.required,
                Validators.pattern(passwordExp)
            ]]
        });
        
        this.username = this.getControls('username');
        this.password = this.getControls('password');
        this.rePassword = this.getControls('rePassword');
    }

    onSubmit() {
        if (!this.formGroup.valid) {
            window.alert('입력값을 확인해주세요.');
            return;
        };
    }
}