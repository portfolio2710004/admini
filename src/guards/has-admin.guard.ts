import { inject } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot } from "@angular/router";

import { tap } from "rxjs";

import { ADMIN_SERVICE } from "../interfaces/admin/admin.service";

// ============================================================================================================
// [로그인 페이지에서 사용되는 가드]
// 관리자 계정이 존재하는지 확인
// 있다면 -> 로그인페이지에 그대로 접근
// 없다면 -> 관리자 생성 페이지로 이동
// ============================================================================================================
export const HasAdminGuard: CanActivateFn = (next: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {

    const router = inject(Router);
    const adminService = inject(ADMIN_SERVICE);

    return adminService.hasAdmin().pipe(
        tap(hasAdmin => !hasAdmin && router.navigate(['/signup']))
    );
}