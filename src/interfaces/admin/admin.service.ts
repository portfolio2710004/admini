import { InjectionToken } from "@angular/core";
import { Observable } from "rxjs";

export interface IAdminService {
    hasAdmin(): Observable<boolean>;
}

export const ADMIN_SERVICE = new InjectionToken<IAdminService>('ADMIN_SERVICE');