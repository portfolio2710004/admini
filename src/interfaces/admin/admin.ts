// ======================================================================================
// [어드민 타입]
// =======================================================================================
export type TAdmin = {
    username: string;
    password: string;
}

// ======================================================================================
// [아이디 정규식]
// - 최소 4자 이상, 최대 8자 이하
// - 영문(대소문자 구분 없음) 및 숫자만을 허용
// =======================================================================================
export const usernameExp = /^[A-Za-z0-9]{4,8}$/;

// ======================================================================================
// [비밀번호 정규식]
// - 최소 8자 이상, 최대 16자 이하
// - 최소 하나의 대문자 포함
// - 최소 하나의 소문자 포함
// - 최소 하나의 숫자 포함
// - 최소 하나의 특수 문자 포함 (!@#$%^&*, 등)
// =======================================================================================
export const passwordExp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,16}$/;
