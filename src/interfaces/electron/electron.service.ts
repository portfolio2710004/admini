export interface IElectronService {
    send(channel: string, data?: any): Promise<any>;
}