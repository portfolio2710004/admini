import { inject } from "@angular/core";
import { AbstractControl, FormBuilder, FormGroup } from "@angular/forms";

// ====================================================================
// 폼 양식을 사용하는 페이지들의 공통 사항 정의
// ====================================================================
export abstract class FormPage {

    protected readonly formBuilder = inject(FormBuilder);

    protected formGroup!: FormGroup;

    protected abstract onSubmit(): void;

    // ========================================================================================
    // FormGroup의 Control을 Null 타입 없이 반환하기 위해 작성
    // - 템플릿에서 옵셔널(?)없이 타입 추론 가능
    // - 잘못 작성될 컨트롤을 사전에 방지
    // ========================================================================================
    getControls(key: string): AbstractControl {
        const controls = this.formGroup.get(key);
        if (!controls) throw new Error(`Cannot find ${key} in formGroup`);
        return controls;
    }
}