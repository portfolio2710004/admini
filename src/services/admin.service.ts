import { Observable, of } from "rxjs";
import { IAdminService } from "../interfaces/admin/admin.service";

export class AdminService implements IAdminService {
    hasAdmin(): Observable<boolean> {
        return of(true);
    }
}