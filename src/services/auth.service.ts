
import { computed, signal } from "@angular/core";
import { Subject } from "rxjs";
import { IAuthService } from "../interfaces/auth/auth.service";

export type TAuthState = {
    accessToken: string | null;
    loaded: boolean;
    error: string | null;
}

export class AuthService implements IAuthService {

    // state
    private readonly state = signal<TAuthState>({
        accessToken: null,
        loaded: false,
        error: null
    });

    // selectors
    readonly accessToken = computed(() => this.state().accessToken);
    readonly loaded = computed(() => this.state().loaded);
    readonly error = computed(() => this.state().error);

    // sources
    readonly signin$ = new Subject();

    constructor() {
        // reducers
    }
}