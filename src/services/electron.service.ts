// src/app/electron.service.ts
import { Injectable } from '@angular/core';

import { ipcRenderer } from 'electron';

@Injectable({
  providedIn: 'root'
})
export class ElectronService {

  send(channel: string, data: any): void {
    if (!ipcRenderer) {
      console.warn('Electron IPC was not loaded');
      return;
    }
    ipcRenderer.send(channel, data);
  }
}
